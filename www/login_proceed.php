<?PHP
session_start();
$sessionID = session_id();

@include("./config/db_connect.php");

$email = strip_tags(htmlspecialchars(htmlentities($_POST["email"])));
$password = strip_tags(htmlspecialchars($_POST["password"]));

$remoteADDR = $_SERVER["REMOTE_ADDR"];
$timestamp = time();

							// http://www.nusphere.com/kb/phpmanual/function.pdostatement-fetch.htm?
$pw_sha1 = SHA1($password); // https://www.thedevfiles.com/2014/08/secure-password-hashing/
							// https://stackoverflow.com/questions/1382215/insert-password-hash-using-pdo-prepared-statements
$stmt1 = $pdo->prepare("SELECT uID, firstname, is_u_admin FROM doit_users WHERE `email` = :bp_email AND `password` = :bp_password");
$stmt1->bindParam(':bp_email', $email);
$stmt1->bindParam(':bp_password', $pw_sha1); // SHA1($password));
$result1 = $stmt1->execute();
$ergs1 = $stmt1->rowCount();

if($ergs1 == 1){
	$result1 = $stmt1->fetch(PDO::FETCH_OBJ);
	$uID = $result1->uID;
	$firstname = $result1->firstname;
	$admin = $result1->is_u_admin;
	
	$stmt2 = $pdo->prepare("SELECT * FROM doit_sessions WHERE `remoteADDR` = :bp_remoteADDR");
	$stmt2->bindParam('bp_remoteADDR', $remoteADDR);
	$result2 = $stmt2->execute();
	$ergs2 = $stmt2->rowCount(); // wurde ein Datensatz mit Bedingung uID und remoteADDR gefunden?
    
	if($ergs2 == 0){
		$_SESSION["uID"] = $uID;
		$_SESSION["firstname"] = $firstname;
		$_SESSION["admin"] = $admin;

		$stmt3 = $pdo->prepare("INSERT INTO doit_sessions SET `uID` = :bp_uID, `sessionID` = :bp_sessionID, `remoteADDR` = :bp_remoteADDR, `timestamp` = :bp_timestamp, `status` = '1'");
		$stmt3->bindParam(':bp_uID', $uID);
		$stmt3->bindParam(':bp_sessionID', $sessionID);
		$stmt3->bindParam(':bp_remoteADDR', $remoteADDR);
		$stmt3->bindParam(':bp_timestamp', $timestamp);
		$result3 = $stmt3->execute();
	
		@Header("Location: ./intern/index2.php");
	}
} else{
	$_SESSION["failure"] = "Kombination aus E-Mail und Passwort unbekannt!";
	$_SESSION["email"] = $email;
	$_SESSION["ergs1"] = $ergs1;
	
	@Header("Location: ./login.php");
}

?>