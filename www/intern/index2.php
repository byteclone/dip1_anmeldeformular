<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Interner Bereich</title>

	<link rel="stylesheet" type="text/css" href="./../css/intern/index2.css">
	
</head>
<body>

<?PHP

@session_start();

@include("./../config/db_connect.php");
@include("./../functions/is_logged_in.php");

$uID = $_SESSION["uID"];
$username = $_SESSION["username"];
$firstname = $_SESSION["firstname"];
$admin = $_SESSION["admin"];

$remoteADDR = $_SERVER["REMOTE_ADDR"];

$is_logged_in = is_logged_in($pdo, $uID, $remoteADDR);

if($is_logged_in == 1){
	echo "<div id='form'>";
	echo "<span>Hallo $firstname, <a href='./logout.php'>logout</a></span><hr/>";
	
	if($admin == 1){
		echo "<h1>Du bist Admin!</h1><a href='./../admin_321q/index.php'>als admin weiter...</a>";
	} else{
		echo "<span><i>Hallo Benutzer. Schade, du hast keine Adminrechte.<br/><br/>Aber herzlichen Glückwunsch, du bist drinn!</i></span>";
	}
	
	echo "</div>";
} else{
	@Header("Location: ./../login.php");
}

?>

</body>
</html>