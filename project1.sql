-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `doit_register`;
CREATE TABLE `doit_register` (
  `regID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(255) NOT NULL,
  `regdate` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`regID`),
  KEY `regID` (`regID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `doit_register` (`regID`, `username`, `firstname`, `lastname`, `email`, `password`, `regdate`, `status`) VALUES
(1,	'byteclone',	'Marius',	'Borchert',	'mariusborchert@googlemail.com',	'',	1516089600,	2),
(2,	'account',	'Demo',	'Account',	'example@example.org',	'',	1516959364,	2);

DROP TABLE IF EXISTS `doit_sessions`;
CREATE TABLE `doit_sessions` (
  `uID` int(11) NOT NULL,
  `sessionID` varchar(255) NOT NULL,
  `remoteADDR` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  KEY `uID` (`uID`),
  CONSTRAINT `doit_sessions_ibfk_3` FOREIGN KEY (`uID`) REFERENCES `doit_users` (`uID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `doit_users`;
CREATE TABLE `doit_users` (
  `uID` int(11) NOT NULL AUTO_INCREMENT,
  `regID` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_u_admin` int(11) DEFAULT NULL,
  `regdate` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`uID`),
  KEY `regID` (`regID`),
  CONSTRAINT `doit_users_ibfk_3` FOREIGN KEY (`regID`) REFERENCES `doit_register` (`regID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `doit_users` (`uID`, `regID`, `username`, `firstname`, `lastname`, `email`, `password`, `is_u_admin`, `regdate`, `status`) VALUES
(2,	2,	'account',	'Demo',	'Account',	'example@example.org',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1,	1516959364,	1);

-- 2018-02-02 08:13:33

